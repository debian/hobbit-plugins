# rpmbuild -D "_builddir $PWD" -ba hobbit-plugins.spec

Name: hobbit-plugins
Version: 20190624
Release: 2
BuildArch: noarch
Summary: plugins for the Xymon network monitor
Requires: perl-File-Slurp

License: GPL, MIT, others

%description
This package provides plugins for the Xymon network monitor.
(Formerly called Hobbit.)

Included client plugins:
 * apt - check for outstanding updates (uses aptitude and dctrl-tools
   if installed)
 * backuppc - check for errors reported by BackupPC servers (needs
   backuppc and sudo)
 * cciss - check hardware RAIDs in HP ProLiant servers (needs
   cciss-vol-status and sudo)
 * cntrk - check the size of the Netfilter Connection Tracking table.
   (needs libfile-slurp-perl)
 * dirtyetc - check for conffiles which are modified compared to
   Debian's defaults but should not be (needs sudo and debsums,
   supports dphys-config)
 * dirtyvcs - check for dirty VCS working copies (supports Git,
   Mercurial, Bzr and Subversion, needs appropriate VCS packages
   and libfile-which-perl installed)
 * dnsq - checks for working DNS on clients (needs libnet-dns-perl
   and libfile-slurp-perl)
 * entropy - check kernel entropy pool size
 * ipmi - read IPMI sensors and event log (needs ipmitool)
 * libs - check for running processes with upgraded libraries (needs
   binutils, lsof, sudo, libyaml-tiny-perl, and libsort-naturally-perl)
 * mailman - checks the existence of Mailman shunt files and aged
   queue files. (needs sudo)
 * mdstat - check for failed or resyncing RAID devices
 * mq - check (postfix's) mail queue (needs libtimedate-perl)
 * misc - meta plugin for running series of scripts (needs
   libipc-run-perl)
 * ntpq - check the ntpd daemon synchronization status (needs ntp)
 * postgres - statistics graphs for PostgreSQL databases (needs
   libdbd-pg-perl)
 * sftbnc - check if the local Postfix MTA has soft_bounce enabled.
 * temp - simple temperature monitor (needs libfile-which-perl and
   libyaml-tiny-perl; depending on the hardware to monitor it optionally
   also needs hddtemp, smartmontools, libxml-twig-perl, nvidia-smi
   (non-free), sudo)

Included server plugins:
 * aptdiff - monitor list of installed packages in host pools
 * conn6 - check IPv6 connectivity (needs fping)
 * ircbot - relay status changes to IRC (needs
   libpoe-component-irc-perl)
 * pgbouncer - monitor pool usage and traffic
 * tftp - checks TFTP servers by downloading a file from them
   (needs libnet-tftp-perl)

Helper software:
 * Hobbit.pm: Perl module for writing plugins
 * xynagios: adaptor for running Nagios plugins with Xymon (needs
   libipc-run-perl)

%setup -c
# Ignore Perl module requirements
%define _use_internal_dependency_generator 0
%define __find_requires /bin/true

%build
make DESTDIR=%{buildroot}

%install
make install DESTDIR=%{buildroot}
sed -i -e 's/#DISABLED/DISABLED/' %{buildroot}/etc/xymon/clientlaunch.d/apt.cfg
sed -i -e 's/DISABLED/#DISABLED/' %{buildroot}/etc/xymon/clientlaunch.d/yum.cfg

%files
/usr/share/perl5/*
/usr/share/man/man3/*
/usr/share/man/man1/*
/usr/lib/xymon/client/ext/*
/usr/lib/xymon/server/ext/*
/usr/bin/*
/etc/apt/apt.conf.d/*
/etc/sudoers.d/*
/etc/xymon/dirty*
/etc/xymon/*yaml
/etc/xymon/misc.d/*
/etc/xymon/xymonlaunch.d/*
/etc/xymon/xymonserver.d/*
/etc/xymon/clientlaunch.d/*
/etc/xymon/graphs.d/*
