#!perl -w

my $xynagios = 'src/usr/bin/xynagios';

use Test::More tests => 15;

# test exit codes
like (`$xynagios true`, qr/green/, "exit 0");
like (`$xynagios false`, qr/yellow/, "exit 1");
like (`$xynagios sh -c 'exit 2'`, qr/red/, "exit 2");
like (`$xynagios sh -c 'exit 3'`, qr/red/, "exit 3");

# test signal catching
like (`$xynagios sh -c 'kill -1 \$\$'`, qr/killed by signal 1/, "catch signal");

# test options
like (`$xynagios --test foo true`, qr/unknown.foo/, "test name");
like (`$xynagios --test foo true --broken`, qr/green/, "argument ordering");

# test trends
like (`$xynagios --trends echo 'OK | foo=5'`, qr/foo : 5/, "trends test");
like (`$xynagios --trends echo 'OK | foo=5'`, qr/echo,foo\.rrd/, "trends rrd name");
like (`$xynagios --trends echo 'OK | foo=5'`, qr/DS:lambda:GAUGE:600:U:U/, "default DS settings");
like (`$xynagios --ds=bar echo 'OK | foo=5'`, qr/DS:bar/, "custom ds name");
like (`$xynagios --dst=DERIVE echo 'OK | foo=5'`, qr/lambda:DERIVE:/, "custom ds type");
like (`$xynagios --heartbeat=1200 echo 'OK | foo=5'`, qr/GAUGE:1200:/, "custom ds heatbeat");
like (`$xynagios --min=-100 echo 'OK | foo=5'`, qr/600:-100:/, "custom ds minimum");
like (`$xynagios --max=100 echo 'OK | foo=5'`, qr/U:100 /, "custom ds maximum");
