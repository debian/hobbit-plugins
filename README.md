Xymon/Hobbit Plugins
====================

This set of client and server plugins (scripts, extensions) for
[Xymon](https://www.xymon.com/) (formerly Hobbit, a monitoring system
compatible with the
[Big Brother](https://en.wikipedia.org/wiki/Big_Brother_(software))
monitoring system) is moderately tailored for Debian, but should be
pretty usable on any *nix platform and also includes checks for
e.g. `yum`.

Some plugins require additional packages installed.

Included client plugins
-----------------------

### apt - check for outstanding updates

#### Optional configuration files

* `/etc/xymon/apt_no_repo_accept` - Packages not from repositories but ok.
* `/etc/xymon/apt_reject` - Packages which must not be installed at all.

Both configuration files should have one package names matching
Perl regular expression per line. The regular expression is
implicitly surrounded by `^` and `$` so a line just containing
`gnome` doesn't match most gnome packages but only the `gnome` meta
package. Use `.*` at the end of the line to match multiple packages
with the same prefix. Please note that this also means that you
have to escape plus-signs (`+`) in package names with backslash
(`\`). See the example below.

#### Opportunistic dependencies

Uses commands from the following packages if installed:

* aptitude
* dctrl-tools

#### Example file

    conkeror
    dphys-swapfile
    unburden-home-dir
    virtualbox-ose-modules-.*
    xlockmore-gl
    xscreensaver.*
    g\+\+

### bkpc - check hosts backed up with BackupPC

Based on the [nagios backuppc check](http://n-backuppc.cvs.sf.net/viewvc/n-backuppc/check_backuppc/check_backuppc?revision=1.21).

In comparison to other included checks this one uses no special
configuration files but needs to be configured via commandline
options in `/etc/xymon/clientlaunch.d/backuppc.cfg`

Run `/usr/lib/xymon/client/ext/backuppc --help` for available options.

#### Needs

* backuppc
* sudo

### cciss - check hardware RAIDs in HP ProLiant servers

#### Needs

* sudo
* cciss-vol-status

### cntrk - check the size of the Netfilter Connection Tracking table.

#### Needs

* libfile-slurp-perl

### dirtyetc - check for conffiles which are modified compared to Debian's defaults but should not be.

#### Configuration file

* `/etc/xymon/dirty_etc_ok` - Which files are ok to be modified.

The file should contain one path match Perl regular expression per
line, with `^` and `$` implied.

#### Example file

    /etc/xymon/clientlaunch.d/dirtyetc.cfg
    /etc/dphys-config
    /etc/unburden_home_dir.*

#### Needs

* sudo (recommended)
* debsums

### dirtyvcs - check for not checked in modifications or files in VCS repositories

Also checks the consistency of (non-bare) git repositories.

#### Configuration files

* `/etc/xymon/dirty_vcs_dirs` - Where to look for repositories.
* `.dirtyvcs_no_fsck` (per git repository)
* `.git/dirtyvcs_no_fsck` (local per git repository)

The file `dirty_vcs_dirs` should contain one directory path per line,
wildcards allowed. In each of the matching directories, it looks
for the typical subdirectories of the supported version control
systems (for now Git, Mercurial, Bzr and Subversion) and queries
the state of the repository.

Paths listed with a leading exclamation mark (`!`) will be
skipped. This allows system administrators to check for their
repos, but skip users' repos. Also supports wildcards there.

The existence of a file named .dirtyvcs_no_fsck (per git
repository, to be committed into the repository) or
`.git/dirtyvcs_no_fsck` (local per working copy) prevents that
dirtyvcs runs `git fsck` on the repository. This may be needed on
big repositories where `git fsck` runs quite some time or needs too
much RAM or I/O.

#### Example file

    /etc
    /etc/*
    /etc/amavis/conf.d
    /opt
    /opt/*
    /opt/*/bin
    /opt/*/sbin
    /usr/local/bin
    /usr/local/sbin
    !/opt/test/bin

#### Needs

* libfile-which-perl

### dnsq - checks for working DNS on clients.

#### Configuration file

* `/etc/xymon/dnsq`

Add one hostname to lookup per file. Will use builtin defaults if
the file doesn't exist.

#### Needs

* libnet-dns-perl
* libfile-slurp-perl.

### ipmi - read IPMI sensors and event log

#### Needs

* ipmitool

### misc - plugin which can aggregate the output and states of other plugins

#### Needs

* libipc-run-perl

#### misc.d/zombies - Warns about processes in zombie state.

##### Configuration file

* `/etc/xymon/zombies_ignore`

Text up to the first whitespace is considered a perl regular
expression (with implicit `^…$` around) to match the name in the
`Name` line of `/proc/*/status` of the zombie's parent process.

If the regular expression contains the string `:^` the text before
this string is a regular expression matched against the user name
of the owner of the process and the text behind is the actual
regular expression matched against the zombie's parent program
name. Use this to ignore e.g. ssh zombies only if they belong to a
specific user.

All text starting with the first non-whitespace after the first
white space is considered the reason for whitelisting these zombie
processes.

##### Example file

See `/usr/share/doc/hobbit-plugins/examples/zombies_ignore`

### libs - check for running processes with upgraded libraries

It is possible to ignore some deleted files either in general or
for specific processes identified by their executable path.  See
`/usr/share/doc/hobbit-plugins/examples/libs.local.yaml` or
`/etc/xymon/libs.yaml` for examples.  Use `/etc/xymon/libs.local.yaml`
to specify local additions to the configurations.  If needed you
can edit `/etc/xymon/libs.yaml` but it is recommended to leave it
alone as it contains the generic exceptions distributed by the
Debian package.

For each process you can list an array of regular expressions.
A process path of `*` indicates regular expressions applied to
any process.

#### Needs

* binutils
* lsof
* sudo
* libsort-naturally-perl
* libyaml-tiny-perl
* libfile-slurp-perl

### mailman - check for the existence of Mailman qfiles.

#### Needs

* sudo

### mdstat - check for failed or resyncing software RAID devices

### megaraid - check state of [LSI MegaRAID SAS controller]()s

A shallow check just consisting of a `/etc/xymon/clientlaunch.d/`
configuration file and an `/etc/sudoers.d/` file as it just calls
`sudo megaclisas-status --nagios` for now.

#### Needs

* sudo
* [megaclisas-status](https://hwraid.le-vert.net/wiki/LSIMegaRAIDSAS#a3.4.megaclisas-status)
  from https://hwraid.le-vert.net/wiki/DebianPackages (not an official
  Debian package, depends on the unofficial, non-free, binary-only
  megacli package from the same APT repository)
* xynagios and its dependendies, see below.

### mq - checks (Postfix's) mail queue

Checks at least Postfix's mail queue by parsing the mailq command's
output. Patches to support other MTAs are welcome.

Recognizes too many mails in the mailq as well as possible RBL
issues.

### net - check network interface states

#### Needs

* ethtool (recommended; needed e.g. for monitoring interface speeds)
* libfile-which-perl
* libfile-slurp-perl
* libipc-run-perl
* libyaml-tiny-perl
* iproute2 (for `ip`) or net-tools (for `ifconfig`)

#### Configuration file

* `/etc/xymon/net.yaml` (or alternatively `/etc/xymon-client/net.yaml`)

The test silently exits and reports nothing if it can't find any
configuration file.

The configuration file format is YAML:

* On the first level, it expects a mapping of hostnames as returned by
  `hostname` (i.e. either a short or a FQDN, depending on the
  configuration of that host). That way you can write a single
  configuration file deployed on many machines and each machine will
  check only those network interfaces it should monitor.

* The second level is either a sequences of interfaces (in that case
  every interface is checked for being `UP`) or another mapping of
  interface names to checks (the third level).

* The third level is either a sequence of _interface checks_ or a
  mapping of check to color (`yellow` or `red`, i.e. the check's
  severity). The color defaults to `yellow` (even if you use the
  mapping syntax with no value), i.e. just a warning.

_Interface Checks_ are basically substring matches on the output of
`ip`, `ifconfig`, or `ethtool` with a few special cases:

* The keywords `Full` and `Half` automatically get `Duplex: `
  prepended to not match `Supported link modes` or `Advertised link
  modes`.

* `FIBRE` and `Twisted Pair` automatically get `Port: ` prepended to
  not match `Supported ports`.

* `TP` automatically gets expanded to `Port: Twisted Pair` as
  `ethtool` (or maybe the kernel) shows an inconsistency and
  sometimes reports `TP` (under `Supported ports`) and sometimes
  `Twisted Pair` (under `Port`).

* `DOWN` is mapped to `not UP` if `ip` is not available as ifconfig
  does not explicitly report `DOWN` and the lack of `UP` is required
  there to test if an interface is down.

* Any number `<n>` followed by either `baseT` (anywhere in the
  _interface check_) or `Mb/s` (with the number being at the beginning
  of the _interface check_) are replaced with `Speed: <n>Mb/s` to not
  match `Supported link modes` or `Advertised link
  modes`.

* Any _interface check_ which starts with `not` followed by one blank
  negates the check, i.e. checks that something is not the case,
  e.g. `not Half` will check that an interface is not half duplex, or
  `not PROMISC` will check that an interface is not in promiscuous
  mode.

The check "knows" for which _interface checks_ `ethtool` or
`/proc/net/dev` is needed and chooses them as backend or data source
automatically. (Feel free to file bug reports if it doesn't recognize
your _interface check_.)

* All _interface checks_ containing one of the following strings are
  checked against the output of `ethtool` with the according interface
  as parameter:
  * `Duplex:`
  * `Speed:`
  * `Auto-negotiation:`
  * `Port:`
  * `Link detected:`
* All _interface checks_ with one of the following strings are checked
  against the amount of packets per 1 second (average over 10 seconds)
  in the according column of `/proc/net/dev` (but using the unique
  names of the `netstat -i` output; trailing colons are fine):
  * `RX-OK`
  * `TX-OK`
  * `RX-ERR`
  * `TX-ERR`
  At least for `…-OK` checks you want to use a comparison operator
  before the value, i.e. `<`, `>`, `<=`, `>=`, or `=` (`==` is
  accepted, too), e.g. `RX-OK >= 100000` or`TX-ERR < 1`.
* All other _interface checks_ are checked against either
  * the combined output of `ip link show dev` and
    `ip address show dev` (plus the according interface name) if `ip`
    is found in `$PATH`.
  * the output of `ifconfig` (plus the according interface name) if
    `ifconfig` is found in `$PATH` and `ip` is _not_ found in `$PATH`.

The check reports an error if neither `ip` nor `ifconfig` are found in
`$PATH` or if an ethtool-requiring _interface check_ is configured,
but `ethtool` is _not_ found in `$PATH`.

#### Caveats

##### Limitations of YAML::Tiny

The used `YAML::Tiny` library doesn't support sequence items which
are of the `- "key": value` format if the key is a quoted
string. This means that some _interface checks_ (like for
`Link detected: yes`) can't be used inside a sequence and with a
color. Use a mapping for the whole interface there.

So instead of using the sequence syntax like this

    somehost:
      eth0:
        - UP
        - "Link detected: yes": red

use the mapping syntax like this:

    somehost:
      eth0:
        UP:
        "Link detected: yes": red

Another caveat is that `"key with spaces": value` doesn't work with
some older version of `YAML::Tiny`, namely those in RHEL 7 and
earlier. In that case try to avoid spaces and if possible write checks
of the type `"RX-OK > 1000000": red` like this: `RX-OK>1000000:
red`. (Less readable, I know.)

#### Examples

##### Example 1:

Check on somehost.example.org that eth0 is up, runs gigabit ethernet
(otherwise issue an alert and not just a warning) with full duplex and
has multicast enabled:

    somehost.example.org:
      eth0:
        - UP
        - 1000baseT: red
        - mtu 1500
        - MULTICAST

##### Example 2:

Check on someothernotfqdnhost that eth0 and eth1 are up:

    someothernotfqdnhost:
      - eth0
      - eth1

##### Example 3:

Check on somefatmonitoringbox that eth0 is up and not in promiscuous
mode, and that bond0 is up, runs in promiscuous mode and runs with 40
gigabit per second:

    somefatmonitoringbox:
      eth0:
        - UP
        - not PROMISC
      bond0:
        UP: red
        PROMISC: red
        40000MB/s: red
        RX-ERR:=0: red
        TX-ERR:=0: red
        RX-OK>100000: red

Additionally the last four entries check that

* no receiver errors happened,
* no transmission errors happened, and
* at least one million packets were received within 10 seconds

on the interface `bond0`.

Issue alerts instead of just warnings if any of the checks for bond0
fails.

### ntpq - check the ntpd daemon synchronization status

#### Needs

* ntp

### postgres - statistics graphs for postgresql databases

#### Needs

* libdbd-pg-perl

### sftbnc - check if the local Postfix MTA has soft_bounce enabled.

### temp - check temperatures of CPU and disks

It is possible to configure individual temperature thresholds. See
`/usr/share/doc/hobbit-plugins/examples/temp.local.yaml` or
`/etc/xymon/temp.yaml` for examples.  Use `/etc/xymon/temp.local.yaml` to
specify local additions to the configurations.  If needed you can edit
`/etc/xymon/libs.yaml` but it is recommended to leave it alone as it
contains the default thresholds distributed by the Debian package.

#### Needs

* libfile-which-perl
* libyaml-tiny-perl

#### Needs for specific temperature checks

Checking disk temperature requires `hddtemp` and either `hddtemp`
running in daemon mode or `sudo` being installed.

Checking disk temperature of disks behind Areca RAID controllers
requires `smartmontools` and `sudo` being installed.

Checking NVidia GPU temperature requires `libxml-twig-perl` and the
(non-free) `nvidia-smi` package being installed. May need `sudo` being
installed, depending on the permissions of `/dev/nvidiactl`.


Non-Debian Plugins
------------------

### yum

Simple shell script which checks the output of `yum check-update` and
sends the result to a Xymon server.

#### Needs

* yum (obviously :-)


Included server plugins
-----------------------

### aptdiff - monitor list of installed packages in host pools

### conn6 - check IPv6 connectivity

Enable this test in `/etc/xymon/xymonlaunch.d/conn6.cfg` and add
`conn6` to the services of a host in `hosts.cfg` and this script will
`ping6` the AAAA record of the given host.

Alternatively you can add a comma separated list of IPv6 addresses or
hostnames that resolve to IPv6 addresses as a parameter to `conn6`, to `ping6`
all these addresses.

#### Examples

    10.1.2.3 foo # ftp conn6:2001:db8:1::42
    10.1.2.4 bar # smtp conn6
    10.1.2.5 baz # conn6:baz.example.com dialup
    10.1.2.6 router # conn6:2001:db8:1::1,2001:db8:2::1

#### Needs

* fping

### ircbot - relay status changes to IRC

#### Needs

* libpoe-component-irc-perl

### tftp - check downloading files from TFTP servers

Enable this test in `/etc/xymon/xymonlaunch.d/tftp.cfg` and add
tftp to the services of a host and this script will try to
download the file `/pxelinux.0` via TFTP from the given host.

Alternatively you can add colon separated a file or path which
should be used to test the TFTP server.

Currently only one file per server can be tested.

#### Examples

    10.1.2.3 foo # ftp tftp
    10.1.2.4 bar # smtp tftp:bootmgr.exe
    10.1.2.5 baz # tftp:/sparc.img

#### Needs

* libnet-tftp-perl


Helper Software
---------------

### Hobbit.pm

Perl module for writing plugins. Used by most plugins listed above,
but also helpful for writing custom plugins.

#### Needs

* perl (obviously :-)

### xynagios

Adaptor for running Nagios plugins with Xymon

#### Needs

* libipc-run-perl


More plugins
------------

 * [Xymonton](http://xymonton.org/) (formerly "The Shire")
 * Many [Big Brother plugins](http://www.deadcat.net/) also run with Xymon
 * The Xymon monitor itself can be found at
   http://xymon.sourceforge.net/ and https://www.xymon.com/
 * A [set of partially Python based Xymon
   plugins](https://github.com/skazi0/xymon-plugins/)
   on GitHub.

Feedback
--------

Feedback welcome, especially suggestions for new plugins. Please use
the [Debian BTS](http://bugs.debian.org/), e.g. via
[reportbug](https://packages.debian.org/reportbug), or e-mail us at
hobbit-plugins@packages.debian.org.
