#!/usr/bin/perl -w

# Copyright (C) 2012 David Bremner <bremner@debian.org>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# CONFIGURATION:
#
# Optionally, create a file /etc/xymon/dnsq containing names to
# lookup, one per line

use strict;
use English;
use Hobbit;
use Net::DNS;
use File::Slurp;

sub fail {
    my $bb = shift;
    $bb->color_line('red', $@);
    $bb->send;
    exit;
}

my $config_file = "/etc/xymon/dnsq";

$ENV{'PATH'} = '/bin:/sbin:/usr/bin:/usr/sbin';
delete @ENV{'IFS', 'CDPATH', 'ENV', 'BASH_ENV'};

my @queries;

if (-f $config_file) {
    @queries = read_file($config_file, chomp => 1);
} else {
    @queries=qw(www.debian.org kernel.org);
}

my $bb = new Hobbit ('dnsq');

my $res = new Net::DNS::Resolver;
fail ($bb, "No resolver") unless ($res);

foreach my $name (@queries) {
    my $packet = $res->search($name);
    if (defined($packet)) {
	my $out = join(" ", $name, map { $_-> rdatastr; } $packet->answer() );
	$bb->color_line('green', $out."\n");
    } else {
	$bb->color_line('red', "$name failed");
    }
}

$bb->add_color ('green');
$bb->send;
