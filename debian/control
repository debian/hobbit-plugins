Source: hobbit-plugins
Section: net
Priority: optional
Maintainer: Christoph Berg <myon@debian.org>
Uploaders: Axel Beckert <abe@debian.org>
Build-Depends: debhelper-compat (= 13),
               libdbd-pg-perl,
               libfile-which-perl,
               libfile-slurp-perl,
               libipc-run-perl,
               liblist-moreutils-perl,
               libmojolicious-perl,
               libnet-dns-perl,
               libnet-tftp-perl,
               libpoe-component-irc-perl,
               libsort-naturally-perl,
               libtimedate-perl,
               libyaml-tiny-perl,
               postgresql-client-common,
               rsync
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/debian/hobbit-plugins.git
Vcs-Browser: https://salsa.debian.org/debian/hobbit-plugins
Homepage: https://salsa.debian.org/debian/hobbit-plugins#readme
Rules-Requires-Root: no

Package: hobbit-plugins
Architecture: all
Depends: xymon-client,
         ${misc:Depends}
Recommends: binutils,
            dctrl-tools,
            libfile-slurp-perl,
            libsort-naturally-perl,
            libyaml-tiny-perl,
            lsb-release,
            lsof,
            sudo
Suggests: apt-config-auto-update,
          bzr,
          cciss-vol-status,
          debsums,
          ethtool,
          fping,
          git,
          ipmitool,
          iproute2 | net-tools,
          libdbd-pg-perl,
          libfile-which-perl,
          libipc-run-perl,
          liblist-moreutils-perl,
          libmojolicious-perl,
          libnet-dns-perl,
          libnet-tftp-perl,
          libpoe-component-irc-perl,
          libpoe-component-sslify-perl,
          libtimedate-perl,
          libxml-twig-perl,
          megaclisas-status,
          mercurial,
          ntp,
          nvidia-smi,
          smartmontools,
          subversion
Enhances: aptitude,
          backuppc,
          dphys-config,
          mailman,
          postfix
Description: plugins for the Xymon network monitor
 This package provides plugins for the Xymon network monitor.
 (Formerly called Hobbit.)
 .
 Included client plugins:
  * apt - check for outstanding updates (uses aptitude and dctrl-tools
    if installed)
  * backuppc - check for errors reported by BackupPC servers (needs
    backuppc and sudo)
  * cciss - check hardware RAIDs in HP ProLiant servers (needs
    cciss-vol-status and sudo)
  * cntrk - check the size of the Netfilter Connection Tracking table.
    (needs libfile-slurp-perl)
  * dirtyetc - check for conffiles which are modified compared to
    Debian's defaults but should not be (needs sudo and debsums,
    supports dphys-config)
  * dirtyvcs - check for dirty VCS working copies (supports Git,
    Mercurial, Bzr and Subversion, needs appropriate VCS packages
    and libfile-which-perl installed)
  * dnsq - checks for working DNS on clients (needs libnet-dns-perl
    and libfile-slurp-perl)
  * entropy - check kernel entropy pool size
  * ipmi - read IPMI sensors and event log (needs ipmitool)
  * kern - check for outdated running kernel and need for reboot (needs
    binutils, libfile-slurp-perl and libsort-naturally-perl)
  * libs - check for running processes with upgraded libraries (needs
    lsof, sudo and libyaml-tiny-perl)
  * mailman - checks the existence of Mailman shunt files and aged
    queue files. (needs sudo)
  * mdstat - check for failed or resyncing RAID devices
  * megaraid - check state of LSI MegaRAID SAS controllers (uses
    xynagios, see below, and additionally needs megaclisas-status from
    https://hwraid.le-vert.net/wiki/DebianPackages and sudo)
  * mq - check (postfix's) mail queue (needs libtimedate-perl)
  * misc - meta plugin for running series of scripts (needs
    libipc-run-perl)
  * net - check network interface states (needs libfile-which-perl,
    libfile-slurp-perl, libipc-run-perl, libyaml-tiny-perl, either
    iproute2 or net-tools and optionally ethtool).
  * ntpq - check the ntpd daemon synchronization status (needs ntp)
  * postgres - statistics graphs for PostgreSQL databases (needs
    libdbd-pg-perl)
  * sftbnc - check if the local Postfix MTA has soft_bounce enabled.
  * temp - simple temperature monitor (needs libfile-which-perl and
    libyaml-tiny-perl; depending on the hardware to monitor it optionally
    also needs hddtemp, smartmontools, libxml-twig-perl, nvidia-smi
    (non-free), sudo)
  * yum - check for outstanding updates on RPM based distros (not
    usable on Debian and derivatives).
 .
 Included server plugins:
  * aptdiff - monitor list of installed packages in host pools
  * conn6 - check IPv6 connectivity (needs fping)
  * ircbot - relay status changes to IRC (needs
    libpoe-component-irc-perl)
  * pgbouncer - monitor pool usage and traffic
  * tftp - checks TFTP servers by downloading a file from them
    (needs libnet-tftp-perl)
 * xcl - checks Lenovo XClarity remote management API for hardware
   failure events (needs libmojolicious-perl and libyaml-tiny-perl)
 .
 Helper software:
  * Hobbit.pm: Perl module for writing plugins
  * xynagios: adaptor for running Nagios plugins with Xymon (needs
    libipc-run-perl)
 .
 As not all plugins are needed by everyone, hard dependencies of
 plugins enabled by default are listed in the Recommends field of the
 package and dependencies of plugins disabled by default or optional
 dependencies of single plugins are listed in the Suggests field -- as
 common with many "collection" style Debian packages. The dependencies
 of each plugin are listed above in parentheses.
